﻿******************
Accesso al sistema
******************

L'accesso alla piattaforma può avvenire tramite:
	- Credenziali LASIS
	- Credenziali locali al sistema

Quando un utente viene aggiunto al sistema tramite le apposite procedure, verrà immediatamente
attvata la possibilità di accedere con entrambe le modalità. Per quanto riguarda l'accesso con LASIS,
l'account è indipendente dal sistema ed è immediatamente associato, quindi l'utente non dovrà compiere 
nessuna azione. Se l'utente volesse attivare le credenziali locali al sistema dovrà procedere alla creazione
di una password tramite il link contenuto nella mail di invito che viene invita in fase di attivazione dell'account.
Il link può essere ignorato se si decide di utilizzare solamente l'accesso basato su Office 365 (LASIS).

.. figure:: images/accesso-sistema.jpg
	:width: 655px
	:align: center
	

Recupero dati di accesso
========================

**ATTENZIONE:** il recupero delle credenziali riguarda solamente l'utilizzo di credenziali locali al sistema. 
Quindi si potrà definire una passowrd locale al sistema e non legata alle credenziali LASIS.

Nel caso si utilizzassero credenziali locali (quindi non Office 365 LASIS) sarà possibile recuperare
i dati di accesso premendo sull'apposito link predisposto nella pagina di login. Verrà richiesto di
fornire l'indirizzo mail e di confermare di la richiesta. 
Se l'indirizzo mail corrisponde ad un utente presente sul sistema, verrà inviato
un messaggio con un link per resettare la password di accesso.

.. figure:: images/accesso-recupero-password.jpg
	:width: 655px
	:align: center

Premendo sul link ricevuto via mail si accederà ad una pagina per impostare la nuova password.
Sarà necessario rispettare le regole riportate nella pagina.
	
.. figure:: images/accesso-nuova-password.jpg
	:width: 655px
	:align: center

