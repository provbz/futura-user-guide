*********************
Referenti di istituto
*********************

La sezione nasce per rendere più facilmente determinabile le funzioni istituzionali rivestite dai vari utenti.
Gli amministratori hanno a disposizione un apposito pannello amministrativo che permette l'estrazione dei referenti in funzione dell'istituto, del plesso,
del ruolo istituzionale rivestito:

.. figure:: images/referenti-amministrazione.jpg
	:width: 655px
	:align: center

A livello di istituto è presente un pannello accessibile al dirigente scolastico e al personale amministrativo che consente l'associazione degli utenti ai vari ruoli istituzionali:

.. figure:: images/referenti-istituto.jpg
	:width: 655px
	:align: center

La colonna di sinistra riporta tutti i ruoli istituzionali. Questi ruoli non rappresentano le funzioni operative presenti nella piattaforma ma le funzioni operative nell'istituto.
Non esistendo una completa sovrapposizione tra ruoli istituzionali e ruoli all'interno della piattaforma è stato necessario separare le due funzioni.
Selezionando un ruolo sulla colonna di sinistra verranno visualizzati gli utenti che rivestono quel ruolo e i relativi plessi in cui l'utente riveste il ruolo.
Sarà possibile aggiungere i nominativi selezionandoli dal menù a tendina.
Se un utente riveste il ruolo su tutti i plessi sarà sufficiente spuntare la colonna "Tutti i plessi".
