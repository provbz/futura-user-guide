****************
Messaggi interni
****************

La sezione amministrativa dei messaggi interni consente agli amministratori di inserire dei messaggi visibili agli utenti dopo aver eseguito il login alla piattaforma.
La sezione è accessibile a tutti gli amministratori ma, ciascuno potrà inserire messaggi solamente in funzione della propria area di competenza.
Allo stesso modo i messaggi inseriti ad esempio da un amministratore della sezione Drop Out potranno essere visualizzati solamente da utenti con accesso alla gestione del Drop Out negli istituti.


Inserimento di messaggi
=======================
L'inserimento dei messaggi avviene tramite l'apposita sezione amministrativa:

.. figure:: images/messaggi-amministrazione.jpg
	:width: 655px
	:align: center

Premendo il pulsante "Nuovo" è possibile inserire nuovi messaggi.

.. figure:: images/messaggi-edit.jpg
	:width: 655px
	:align: center

Gli amministratori che si occupano della gestione di più aree avranno a disposizione la possibilità di selezionare l'area di competenza del messaggio.
Ogni messaggio può essere riferito ad una o più aree di competenza.
Le aree attualmente supportate sono:

    * PEI - PDP
    * Drop out
    * Mondo delle parole progetto letto scrittura
    * Portfolio docenti
    * Educazione salute
    * Modello E


Visualizzazione dei messaggi
============================
I messaggi inseriti dagli amministratori saranno visibili dagli utenti dopo il login nella prima pagina visualizzata:

.. figure:: images/messaggi-visualizzazione.jpg
	:width: 655px
	:align: center

La visualizzazione sarà limitata ai soli messaggi relativi alle aree di accessibilità dell'utente.
