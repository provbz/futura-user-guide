﻿futura-user-guide: Indice
=========================

La piattaforma Futura è costituita da un insieme di moduli strutturati per supportare la creazione di differenti documenti.
Il presente manuale raccogle le funzionalità attualmente offerte.

.. toctree::
	:maxdepth: 3
	
	accesso-al-sistema
	amministrazione-tecnica
	messaggi-interni
	gestione-delle-strutture
	referenti-di-istituto
	modulo-pei-pdp
	modulo-educazione-salute
	modulo-portfolio-docenti
	modulo-drop-out
	modulo-mondo-delle-parole
	modulo-modello-e
	rilevazione-cattedre
