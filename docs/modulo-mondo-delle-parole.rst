********
Mondo delle Parole e Progetto Letto-Scrittura
********

Il modulo consente di raccogliere i dati di test didattici somministrati nelle scuole dell'infanzia e della primaria.

Sezione amministrativa
======================
La sezione amministrativa di questo modulo è accessibile agli utenti con ruolo "Amministratore Mondo delle Parole progetto Letto-Scrittura".
All'interno del pannello amministrativo è possibile trovare le seguenti funzionalità:

    * Test: con l'elenco dei test presenti in piattaforma.
    * Prove: con l'elenco e la possibilità di creare nuove somministrazioni nel sistema.
    * Importa dati studenti: la sezione consiste di caricare i dati degli studenti a partire da un file excel con struttura prefissata.
    * Alfa: accesso ai dati storici della piattaforma Alfa
    * Esis: accesso ai dati storici della piattaforma Esis


Test
____________________________
La sezione test riporta l'elenco dei test disponibili nella piattaforma.
Attualmente sono presenti 19 test in piattaforma per la classe 3* della scuola dell'infanzia, 1* e 2* primaria di primo grado.
Per ogni test è possibile specificare i dati base: titolo, classe e periodo, il codice e le note di compilazione.
Inoltre, è possibile allegare i materiali e le istruzioni.

.. figure:: images/admin-mondo-parole-test.jpg
	:width: 655px
	:align: center

Il sistema consente inoltre di caricare le soglie di valutazione per determinare gli studenti in fascia:

    * Rossa: richiesta di intervento
    * Gialla: richiesta di attenzione
    * Verde: prestazione adeguata

Per ogni test la piattaforma consente inoltre di elaborare i dati presenti derivando i centili e eventualmente valutare l'introduzione di nuove soglie.

.. figure:: images/admin-mondo-parole-soglie.jpg
	:width: 655px
	:align: center


Prove
----------------------------
La sezione prove consente di pianificare le somministrazioni che dovranno poi essere compilate dalle scuole presenti in piattaforma.
Ogni somministrazione viene associata ad una specifica classe e periodo. Vengono inoltre definite la data di inizio e di fine della somministrazione e i test da somministrare.
Solo nel periodo compreso tra la data di inizio e fine gli insegnanti potranno caricare i risultati.

.. figure:: images/admin-mondo-parole-prove.jpg
	:width: 655px
	:align: center

Per ogni test associato ad una somministrazione sarà possibile specificare il set di soglie da applicare e eventuali parametri dinamici (per i test che li precedono) dipendenti dalle somministrazioni.
Infine, la sezione consente di accedere ai risultati cumulativi delle somministrazioni effettuate.



Gestione a livello di istituto
==============================
A livello di istituto devono essere predisposti uno o più utenti con il ruolo "Referente di istituto per Mondo delle Parole e Progetto Letto-Scrittura".
Sarà fondamentale associare a questi utenti il grado scolastico. Infatti, i referenti potranno vedere solo le somministrazioni/prove che appartengono al loro grado scolastico.
Come per gli altri moduli, i dati presenti in questa sezione sono sempre consultabili dal dirigente scolastico.

Elenco prove
_______________________
L'elenco delle somministrazioni è visibile nella sezione "Mondo delle parole progetto Letto-Scrittura" accessibile dal menù principale della piattaforma 
o dal pulsante presente in home page per gli utenti che hanno diritto di visualizzare i dati contenuti.

.. figure:: images/structure-mondo-parole-prove.jpg
	:width: 655px
	:align: center

Ogni utente vedrà solamente le somministrazioni per le quali ha diritto di accesso.
Su ogni riga della tabella compaiono due icone:

    * Freccia: consente l'accesso alla somministrazione.
    * Grafico: accessibile solo al coordinatore di istituto e al dirigente, riporta l'andamento complessivo di tutti i plessi dell'istituto.

.. figure:: images/structure-mondo-parole-risultati-plessi.jpg
	:width: 655px
	:align: center


Elenco plessi e classi
______________________
Accedendo ad una prova la pagina verrà mostrata divisa in due colonne:

.. figure:: images/structure-mondo-parole-prove-classi.jpg
	:width: 655px
	:align: center

La colonna di sinistra riporta lo stato della prova:

    * In attesa di apertura: stato giallo, la data di inizio della prova non è ancora stata raggiunta. Sarà possibile controllare l'elenco delle classi, degli studenti e scaricare i materiali per la somministrazione.
    * Prova aperta: stato verde, la prova è in corso, i dati sono caricabili.
    * Chiusa: stato rosso, i dati rimangono consultabili ma non sarà più possibile eseguire modifiche o caricamenti.

I dati generali della prova: data di apertura, chiusura, classe e periodo.
I dati dei test che verranno eseguiti completi di tutti i materiali e le istruzioni necessarie per i somministratori.
Nella sezione di destra vengono visualizzati i plessi.
Il referente di istututo ha la possibilità di specificare uno o più referenti di plesso selezionando i nominativi degli insegnanti dal menù a tendina posto sotto il nome di ciascun plesso.
L'elenco delle classi di ciascun istituto e la possibilità di inserire nuove classi.
Inoltre è disponibile l'accesso ai dati statistici di istituto. Questi dati riepilogano l'andamento di tutte le classi dell'istituto.
Cliccando sul nome di una classe si accede alla sezione di gestione della classe.


Gestione della classe
---------------------
La gestione classe mantiene la struttura a due colonne descritta in precedenza. Nella colonna di sinistra i referenti di plesso e i referenti di istituto potranno specificare gli insegnanti di classe
selezionando i nominativi degli utenti dal menù a tendina. Gli insegnanti di classe potranno accedere ai dati della classe in sola visualizzazione.
Da questa sezione é possibile gestire i dati degli studenti modificando le informazioni anagrafiche o aggiungere/rimuovere nominativi per fare in modo che l'elenco rispecchi la struttura della classe.
Inoltre, è possibile trasferire gli studenti verso altri istituti.
Al piede dell'elenco degli studenti sono presenti i pulsanti che permettono di accedere al caricamento dei dati delle singole prove.

.. figure:: images/structure-mondo-parole-classe.jpg
	:width: 655px
	:align: center


Caricamento dati
----------------
Ogni test implementato nel sistema offre un'interfaccia di caricamento personalizzata in base alle caratteristiche del test stesso.
Esistono però una serie di elementi comuni:

.. figure:: images/structure-mondo-parole-caricamento-risultati.jpg
	:width: 655px
	:align: center

Ogni riga di caricamento riporta i dati di uno studente.
Le prime quattro colonne sono comuni per tutti i caricamenti:

    * Nome dello studente cliccabile. Cliccando sul nome vengono visualizzati i dati di anagrafica e il riepilogo di tutte le precedenti valutazioni presenti per lo studente.
    * Assente: permette di specificare se lo studente era assente durante la somministrazione del test.
    * Non valido: permette di specificare se il test non era valido, ad esempio, nel caso in cui lo studente si rifiuti di eseguire la prova o ci sia qualche tipo di impedimento.
    * Eseguito: il test è stato eseguito, si procederà quindi al caricamento dei dati e alla valutazione

Alcune colonne della tabella di caricamento presentano dati calcolati con i risultati della prova.
Se per il test sono state impostate delle soglie i risultati si coloreranno di:

    * rosso: richiesta di intervento.
    * giallo: richiesta di attenzione.
    * verde: risultati adeguati.


.. figure:: images/structure-mondo-parole-risultati-dettaglio-studente.jpg
	:width: 655px
	:align: center
