﻿*****************
Portfolio docenti
*****************

Il modulo del Portfolio Docenti consente di gestire il portfolio degli insegnanti in anno di prova.

Sezione amministrativa
======================
Gli utenti con ruolo "Amministratore portfolio docenti" possono accedere al pannello amministrativo dedicato. L'accesso non è possibile sui dati
degli istituti dato che a livello di istituto l'accesso alla visualizzazione è gestito dal pannello degli utenti di istituto.
Il pannello amministrativo per il modulo consente di visualizzare l'elenco degli insegnenti in anno di prova attualmente censiti sul sistema.

.. figure:: images/admin-portfolio-docenti.jpg
	:width: 655px
	:align: center


Gestione a livello di istituto
==============================
A livello di istituto sarà il dirigente o un amministrativo a poter creare i docenti con ruolo "Insegnante in anno di prova".
Gli utenti con questo ruolo avranno la possibilità di caricare i documenti del proprio portfolio.
I Dirigenti e gli Amministrativi avranno la possibilità di visualizzare una parte del portfolio, quella espresamente 
pensate per la condivisione di informazioni con il dirigente.


Accesso al portfolio
--------------------
L'accesso al portfolio può avvenire da parte del dirigente tramite l'icona che compare nel riepilogo degli utenti quando
un utente è impostato con il ruolo "Insegnante in anno di prova".

.. figure:: images/structure-portfolio-users.jpg
	:width: 655px
	:align: center

La gestione del portfolio permette agli insegnanti di scaricare modelli di files e caricare i dati compilati.

.. figure:: images/structure-portfolio.jpg
	:width: 655px
	:align: center
