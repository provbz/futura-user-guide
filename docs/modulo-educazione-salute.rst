﻿**********************************
Progetti di educazione alla salute
**********************************

Il modulo consente di gestire i progetti di Educazione alla Salute.

Sezione amministrativa
======================
Gli utenti con ruolo "Amministratore Educazione alla Salute" possono accedere al pannello amministrativo dedicato e ai dati presenti negli
Istituti relativi ai progetti caricati dai referenti.
Il pannello amministrativo per il modulo consente di visualizzare e ricercare i dati per tutti le richieste caricate negli istituti.
Viene fornito un export dei dati generali e una sezione di riepilogo che raccoglie il numero di alunni, classi e ore coinvolte per ogni istituto.

.. figure:: images/admin-educazione-salute.jpg
	:width: 655px
	:align: center


Gestione a livello di istituto
==============================
A livello di istituto gli utenti coinvolti nella compilazione sono:
	* Referente Educazione alla Salute
	* Referente Cyber Bullismo

Inoltre, come per gli altri moduli, il dirigente di istituto e gli amministrativi hanno accesso alla visualizzazione dei documenti.

Elenco documenti
----------------
A livello di istituto è possibile per gli utenti autorizzati visualizzare l'elenco dei progetti caricati, modificare i dati e crearne di nuovi.s

.. figure:: images/structure-ed-salute.jpg
	:width: 655px
	:align: center
