﻿*********
Modello E
*********
Il modulo consente di gestire la compilazione del modello E utilizzato per la richiesta di risorse e di sostegno.

Sezione amministrativa
======================
Gli utenti con ruolo "Amministratore Modello E" possono accedere al pannello amministrativo dedicato e ai dati presenti negli
Istituti relativi ai casi caricati dai referenti.
Il pannello amministrativo per il modulo consente di visualizzare e ricercare i dati per tutti le segnalazioni caricate negli istituti.
Inoltre, sono state predisposte le seguenti dashboard:

    * Riepilogo delle segnalazioni per grado scolastico
    * Riepilogo per genere
    * Riepilogo per cittadinanza

I dati delle dashboard vengono calcolati in tempo reale ad ogni nuovo accesso.

.. figure:: images/admin-drop-out.jpg
	:width: 655px
	:align: center


Gestione a livello di istituto
==============================
A livello di istituto gli utenti coinvolti nella compilazione del PEI e del PDP sono:

	* Referente di classe per il modello Es

Inoltre, come per gli altri moduli, il dirigente di istituto ha accesso alla visualizzazione dei documenti.


Elenco studenti segnalati
-------------------------
A livello di istituto è possibile per gli utenti autorizzati visualizzare l'elenco degli studenti per i quali viene compilato il Modello E.

.. figure:: images/structure-e-model.jpg
	:width: 655px
	:align: center

È possibile creare nuove segnalazioni premendo il pulsante "Nuovo" e inserendo il codice POPCORN dello studente.
Se il sistema contiene già i dati dello studente verrà presentata l'anagrafica compilata.
Se il codice dello studente è associato ad un altro istituto verrà proposta la procedura di richiesta di trasferimento.

Importazione da anni precedenti
-------------------------------
Al passaggio di anno scolastico l'elenco dei documenti comparirà vuoto. Questo perchè, l'elenco principale
visualizza i documenti in fase di compilazione per l'anno corrente.
È possibile recuperare la lista completa dei documenti compilati l'anno precedente premendo il pulsante "Recupera".
In alternativa è possibile premere il pulsante "Nuovo" e inserire il codice popcorn dello studente.
Il sistema individuerò il documento compilato l'anno precedente e consentirà di recuperare i dati o creare una nuova versione
a partire da un documento vuoto.
