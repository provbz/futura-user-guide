﻿************************
Gestione delle strutture
************************

Gli amministratori di sistema hanno la possibilità di creare gli Istituti e associare a ciascun istituto i Dirigenti scolastici e gli Amministrativi di Istituto.
I dirigenti di Istituto e gli Amministrativi avranno i compiti fondamentali di:

	* Creare e mantenere aggiornata la lista degli utenti dell'istituto;
	* Gestire le richieste di trasferimento;
	* Verificare ed eventualmente aggiornare la lista dei plessi all'interno del proprio istituto;


Gestione degli utenti della struttura
=====================================
I dirigenti di istituto e gli Amministrativi di istituto hanno il compito di creare gli accessi per tutti gli utenti dell'istituto. 
Chi ha il ruolo di Dirigente di istituto o Amministrativo potrà anche nominare altri Dirigenti e Amministrativi
per poter suddividere il lavoro di inserimento se fosse necessario o per consentire l'accesso a personale che necessità di poter
verificare i dati inseriti nei documenti presenti a bordo piattaforma.
Dal menù in alto nella pagina premere "Utenti", la pagina riporta tutti i nominativi degli utenti che accedono all'istituto.
È fondamentale mantenere agiornata la lista per garantire l'accesso ai dati solo ed esclusivamente agli utenti che ne hanno necessità e diritto.
Se si desidera aggiungere un nuovo utente premere "Nuovo". Verrà richiesto l'indirizzo email e il ruolo da associare al nuovo utente.
Se l'utente da aggiungere non è mai stato censito saranno richieste alcune informazioni aggiuntive.
Completato l'inserimento, l'utente riceverà una mail di invito con un link per la generazione della password di accesso.
Se l'utente desidererà acedere con credenziali Office 365 non sarà necessario definire la nuova password.

.. figure:: images/struttura-elenco-utenti.jpg
	:width: 655px
	:align: center

I ruoli selezionabili in fase di creazione del nuovo utente sono strettamente correlati ai moduli implementati nel sistema.
Di seguito l'elenco completo:

	* Dirigente di istituto
	* Referente BES
	* Insegnante PEI-PDP
	* Amministrativi
	* Collaboratore all'integrazione: accesso alla sezione PEI-PDP
	* Insegnante anno di prova: accesso al caricamento del proprio portfolio
	* Coordinatore Educazione alla salute
	* Referente Cyberbullismo: accede alla sezione di educazione alla salute
	* Referente Modello E
	* Coordinatore di classe per il Drop Out
	* Insegnante: non ha diritti specifici, viene utilizzato per poter importare i dati di un utente e associare eventuali responsabilità

In fase di edit dei dati di un utente sarà inoltre possibile definire:
	* Grado scolastico di riferimento
	* Plesso di competenza con responsabilità nel plesso, si veda la figura sotto

.. figure:: images/structure-user-edit.jpg
	:width: 655px
	:align: center

L'associazione degli utenti alla loro responsabilità nel plesso può avvenire in modo più rapido dalla sezione Referenti di Istituto.

Gestione dei trasferimenti
==========================
Solo i Dirigenti e gli Amministrativi hanno la possibilità di gestire le richieste di trasferimento. Una richiesta di trasferimento
avviene nel caso in cui uno studente si trasferisse da un istituto ad un altro tra le scuole gestite dalla piattaforma.
Ad esempio: se l'utente identificato dal codice POP_10 è attualmente iscritto all'istituto Bolzano 1 e per qualsiasi motivo si trasferisse
nell'IC Bolzano 2, gli insegnanti dell'IC Bolzano 2 dovranno compilare il documento che era già in carico alla scuola precedente.
Per procedere alla compilazione l'insegnante di Bolzano 2 chiederà di poter gestire lo studente POP_10. Tale richiesta di gestione dovrà
essere approvata dalla scuola di origine. 
L'approvazione delle richieste è quindi un processo fondamentale per consentire l'utilizzo efficiente del sistema.
La gestione delle richieste di trasferimento avviene accedendo all'apposita sezione "Richieste" del menù principale.

.. figure:: images/structure-trasferimento-gestione.jpg
	:width: 655px
	:align: center
	
Premendo sul pulsante "Gestisci" associato ad ogni riga di richiesta si aprirà una pagina in cui impostare l'esito della richiesta.
Gli esiti possibili sono:

* Approvata: La gestione viene trasferita e la nuova scuola riceve i diritti di compilazione del documento.
* Approvata solo per gestione: In questo caso la nuova scuola riceverà il controllo sui dati dello studente ma non riceverà il documento. Dovrà quindi compilare un nuovo documento.
* Rifiutata: La richiesta viene rifiutata dalla scuola di origine. L'unico motivo ritenuto valido è per richieste non giustificate. Ad esempio per codici studente che sono ancora in gestione presso la scuola.

Per ogni stato è possibile inserire una nota da inviare alla scuola di destinazione.
	
.. figure:: images/structure-trasferimento-approvazione.jpg
	:width: 655px
	:align: center

Oltre alla gestione di trasferimento il sistema implementa la possibilità di inviare i dati da parte degli utenti
che conoscono già la destinazione di un utente. Questo processo non è soggetto ad approvazione dato che si tratta
di un invio fatto a partire dal titolare dei dati.
