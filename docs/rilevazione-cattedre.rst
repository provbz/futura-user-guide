*********************
Rilevazione dati finalizzata alla distribuzione delle cattedre A023ter
*********************

Il modulo consente di caricare ad ogni istituto i dati di richiesta per la distribuzione delle cattedre A023ter.

Sezione amministrativa
======================
La sezione amministrativa della rilevazione cattedre è consentita agli utenti di piattaforma con ruolo "Amministratore rilevazione cattedre A023ter".
In questa sezione gli amministratori possono visualizzare, ricercare e scaricare i dati caricati dai vari istituti.

Gestione a livello di istituto
=============================
A livello di istituto gli utenti coinvolti nella compilazione della rilevazione cattedre sono:
    * Dirigente scolastico
    * Amministrativo

Gli utenti con questi ruoli possono eseguire una compilazione all'anno del modulo predisposto.
Accedendo successivamente al modulo, i campi risulteranno in sola lettura:

.. figure:: images/rilevazione-cattedre-form.jpg
	:width: 655px
	:align: center
