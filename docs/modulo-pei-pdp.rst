﻿**************
Modulo PEI, PEI du tirocinio, PDP, PDP per bisogni linguistici
**************

Il modulo consente di compilare il PEI e il PDP per uno studente.

Sezione amministrativa
======================
La sezione amministrativa del PEI-PDP è consentita agli utenti di piattaforma con ruolo "Amministratore PEI-PDP".
All'interno della sezione amministrativa è visibile l'elenco dei documenti compilati nella piattaforma.
L'amministratore PEI-PDP ha anche la possibilità di accedere alle sezioni di PEI e PDP dei singoli istituti.

Gestione a livello di istituto
==============================
A livello di istituto gli utenti coinvolti nella compilazione del PEI e del PDP sono:
	* Referente BES
	* Insegnante PEI-PDP
	* Collaboratori all'integrazione

Inoltre, come per gli altri moduli, il dirigente di istituto e gli amministrativi hanno accesso alla visualizzazione dei documenti.


Elenco documenti
----------------
Dopo aver eseguito l'accesso, gli utenti abilitati potrenno accedere all'elenco dei documenti compilati (sia PEI che PDP).
Mentre il Referente BES e il Dirigente avranno la vasione complessiva di tutti gli studenti caricati, gli insegnanti e i collaboratori all'integrazione
potranno visualizzare solo i documenti che sono stati condivisi con loro.


Stato file
----------
Partendo dall'elenco dei documenti è possibile recuperare una dashboard contenente lo stato del caricamento di tutti i file per ciascuno studente.
Questa sezione consente di avere una rapida panoramica sullo stato di lavorazione dei documenti degli studenti.


Nuovo documento
----------------
Pre procedere alla compilazione di un nuovo documento sarà sufficiente premere il pulsante "Nuovo".
Il sistema richiederà il codice identificativo dello studente:

.. figure:: images/structure-studente-nuovo.jpg
	:width: 655px
	:align: center

Se il sistema contiene già un documento compilato per lo studente ma tale documento è stato gestito da un'altra
scuola si potrà procedere ad una richiesta di trasferimento. I tempi di approvazione delle richieste dipendono
dalle scuole.

.. figure:: images/structure-studente-richiesta-di-trasferimento.jpg
	:width: 655px
	:align: center
	
Se invece il codice dello studente non è presente nel sistema sarà richiesto di compilare i dati dell'anagrafica.
Alcuni dati potrebbero già essere presenti se sono stati creati documenti di altro tipo per lo stesso identificativo.

.. figure:: images/structure-studente-nuovo-anagrafica.jpg
	:width: 655px
	:align: center
	
Una volta completata la compilazione dei dati di base sarà visualizzata la pagina di dettaglio dello studente con le funzioni
specifiche di compilazione e gestione.
In particolare si avrà la possibilità di gestire i seguenti elementi:

	* Edit dei dati di anagrafica;
	* Condivisione con altri insegnanti: la funzionalità permette di fornire l'accesso ai dati dello studente ad altri insegnanti;
	* Diagnosi: inserimento dei dati di diagnosi;
	* Patto: gestione del patto con la famiglia;
	* Calendario: calendario delle attività settimanali;
	* Rimuovi: rimozione dei dati dello studente;
	* caricamento di allegati;
	* Funzione di elaborazione: per la compilazione del PEI e del PDP;
	* Stampa ed esporta: per scaricare il documento in formato ODT;

.. figure:: images/structure-studente-home.jpg
	:width: 655px
	:align: center


Importazione da anni precedenti
-------------------------------
Al passaggio di anno scolastico l'elenco dei documenti comparirà vuoto. Questo perchè, l'elenco principale
visualizza i documenti in fase di compilazione per l'anno corrente.
È possibile recuperare la lista completa dei documenti compilati l'anno precedente premendo il pulsante "Recupera".
In alternativa è possibile premere il pulsante "Nuovo" e inserire il codice popcorn dello studente.
Il sistema individuerò il documento compilato l'anno precedente e consentirà di recuperare i dati o creare una nuova versione
a partire da un documento vuoto.

	
Elaborazione del PEI e del PDP
------------------------------
Cliccando sul pulsante "Elabora" presente nella scheda di detaglio dello studente è possibile procedere alla compilazione
dei documenti (PEI o PDP).
	
.. figure:: images/structure-pei-elabora.jpg
	:width: 655px
	:align: center
	
Le voci inserite nel documento devono essere associate agli elementi di tassonomia previsti dal sistema. Sarà quindi necessario 
premere sul pulsante "Ricerca" per poter esplorare la tassonomia e selezionare le voci da elaborare.

.. figure:: images/structure-pei-cerca.jpg
	:width: 655px
	:align: center
	
Una volta trovata la voce su cui si desidera lavorare premere sul pulsante "Aggiungi" per inserire il riferimento nel PEI/PDP dello
studente. Fin quando le voci non saranno completate con obiettivi e attività sarà visualizzato unsegnale di attenzione.

.. figure:: images/structure-pei-elabora-2.jpg
	:width: 655px
	:align: center

Premendo su una voce dell'elenco (un processo) si accederà alla sezione di dettaglio. Premendo Aggiungi si potranno associare obiettivi e attività.
	
.. figure:: images/structure-pei-elabora-voce.jpg
	:width: 655px
	:align: center


Stampa ed esporta
-----------------
La sezione consente di scaricare il documento completo, tramite questa pagina sarà inoltre possibile specificare il nome dell'autore
che ha redatto il documento, delle note da inserire nella pagina iniziale, la data dell'ultima revisione e selezionare le sezioni da
esportare.
È possibile scaricare il documento in formato doc o odt.

.. figure:: images/structure-pei-export.jpg
	:width: 655px
	:align: center

Si consiglia di esportare il file in formato .odt e di aprirlo usando LibreOffice. 
Qualora fossero attive le revisioni (sistema che tiene traccia delle modifiche al documento), si consiglia di andare nel menu Modifica - Revisioni - Accetta tutto 
e poi di levare la spunta dalla voce Registra sempre nello stesso sottomenu.

.. figure:: images/libre-office-review.jpg
	:width: 655px
	:align: center

