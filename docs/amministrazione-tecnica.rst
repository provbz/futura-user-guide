﻿***********************
Amministrazione tecnica
***********************

L'amminitrazione tecnica è riservata agli utenti con ruolo "Amministratore". 
Gli utenti con questo ruolo hanno visibilità completa sull'intero sistema e, quindi, accesso illimitato a tutti i moduli.
Il ruolo amministrativo da diritto a gestire le seguenti funzionalità fondamentali:

* Amministrazione di aspetti tecnici di configurazione;
* Gestione degli account utente: gli amministratori hanno il compito di fornire l'accesso agli utenti;
* Monitoraggio e assistenza: la visibilità su tutti i dati, consente agli amministratori di eseguire operazioni di assistenza accedendo ai documenti e alle informazioni dei singoli istituti;


Gestione utenti
===============
Dopo aver eseguito il login al sistema un amministratore avrà a disposizione la sezione
di gestione degli utenti. Tale sezione costituisce la pagina di atterraggio per gli utenti con 
questo ruolo.
La sezione consente di:

* Ricercare utenti registrati sul sistema;
* Inserire nuovi utenti: si suggerisce di usare la sezione solo per gli utenti amministratori. Si veda sotto per la gestione dei dirigenti;
* Modificare la password di un utente in modo forzato;
* Modificare i dati di un utente: indirizzo mail, nome e cognome. Si ricorda che l'indirizzo mail viene utilizzato nell'installazione di Bolzano anche per l'associazione all'account Office 365;
* Disabilitare l'accesso;

.. figure:: images/amministrazione-gestione-utenti.jpg
	:width: 655px
	:align: center


Gestione Strutture
==================
Le strutture rappresentano aggregazione di utenti e documenti. In fase di pianificazione va decisa
la granularità di riferimento. Ad esempio, una struttura può rappresentare un Istituto Comprensivo, una Scuola o un Plesso.
Nell'installazione di Bolzano si è deciso di far corrispondere la struttura ad un Istituto Comprensivo.

La sezione di gestione strutture permette di creare, modificare e rimuovere le strutture dal sistema.
La messa in servizio del sistema richiede come fase iniziale la creazione di tutte le strutture che richiedono di utilizzare
il sistema.

Gli amministratori potranno spostarsi attraverso i dati delle varie strutture attraverso il menù a tendina posto in alto 
sulla pagina.

Dopo aver creato una struttura l'amministratore dovrà accedere al tab Utenti per associare il dirigente di istituto o
gli Amministrativi di istituto.

.. figure:: images/amministrazione-gestione-strutture.jpg
	:width: 655px
	:align: center

Ogni struttura può contenere al suo interno ulteriori sotto-strutture. Nell'installazione realizzata le sotto-strutture 
rappresentano i plessi all'interno degli istituti complessivi. 
A titoli di esempio la seguente immagine con un istituto con realtivi plessi:

.. figure:: images/amministrazione-gestione-strutture-plessi.jpg
	:width: 655px
	:align: center

Per ogni istituto comprensivo sono stati aggiunti dei metadati descrittivi che descrivono l'istituto e abilitano
funzionalità specifiche:

.. figure:: images/amministrazione-gestione-strutture-edit.jpg
	:width: 655px
	:align: center


trasferimenti
=============
Per consentire di verificare la situazione delle richieste di trasferimento tra i vari istituti è stato introdotto un nuovo pannello amministrativo
che consente di consultare rapidamente lo stato di tutti i trasferimento in corso ed eseguiti all'itnerno del sistema.

	
Ruoli
=====
La sezione di gestione ruoli permette di associare i permessi di accesso ai ruoli applicativi.
La soluzione presenta due macro tipologie di ruolo:

* Ruolo globale: rappresenta il ruolo di un utente sull'intero sistema dopo aver eseguito il login;
* Ruolo di struttura: rappresenta il ruolo di un utente una volta che seleziona una delle strutture a cui è associato.

In seguito all'estensione della piattaforma sono stati aggiunti ruoli a livello di plesso e di classe. In particolare questi ruoli hanno consentito di gestire le
autorizzazioni nel modulo "Mondo delle parole".

I ruoli previsti dall'applicazione sono:

* Globali:
	* Amministratore: con accesso completo a tutte le funzionalità;
	* User: gli utenti non amministrativi;
	* Amministratore drop out: accesso alla sezione amministrativa del Drop Out;
	* Amministratore Educazione Salute: accesso alla sezione amministrativa di educazione alla salute;
	* Amministratore Modello E: amministratore del Modello E;
	* Amministratore Mondo Delle Parole e Progetto Letto-Scrittura: amministratore per Mondo delle Parole e Progetto Letto Scrittura;
	* Amministratore PEI-PDP: amministratore PEI-PDP;
	* Amministratore Portfolio Docenti;
	* Amministratore servizio valutazione: consente l'accesso al modulo esterno del Servizio di Valutazione;
	* Amministratore Strutture: consente la gestione dell'elenco delle strutture/istituti;
	* Amministrazione utenti IC: amministratore degli utenti associati ad istituti;
	* API: dati utente lettura: ruolo usato per accedere alle API di integrazione;
	* Supervisore servizio valutazione: accesso al sistema collegato del servizio di valutazione;
	* User: gli utenti non amministrativi;
	* Supervisore Servizio di valutazione;
	* Amministratore Servizio di valutazione;
	* Amministratore Portfolio Docenti;
* Di struttura:
	* Amministrativi: come i Dirigenti di Istituto;
	* Collaboratore all'integrazione;
	* Coordinatore Educazione alla Salute;
	* Dirigente di istituto: ha completo controllo su tutta una struttura;
	* Docente servizio valutazione;
	* Insegnante: ha visibilità sui propri documenti che può condividere con altri insegnanti dell'istituto, non può creare utenti;
	* Insegnante anno di prova;
	* Insegnante PEI-PDP;
	* Referente BES: il referente BES viene nominato da utenti amministrativi o dai dirigenti di istituto e può creare gli insegnanti;
	* Referente Cyberbullismo;
	* Referente di istituto per il drop out;
	* Referente di istituto per Mondo delle Parole e Progetto Letto-Scrittura;
	* Referente di plesso per il drop out;
	* Referente Modello E;
	* Segreteria servizio valutazione;
	* Studente: si tratta di un ruolo che non da accesso a funzionalità aggiuntive ma permette di gestire gli studenti;
	* Nucleo interno di valutazione;
	* Referente interculturale;
	* Docente Prova di Tedesco L2 (SV);
	* Segreteria Prova di Tedesco L2 (SV);
	* Insegnante;
	* Referente Modello E;
	* Amministrativi;
	* Dirigente di Istituto: permette l'accesso a tutte le funzionalità a livello di struttura;
* Di plesso:
	* Referente plesso per Mondo delle Parole e Progetto Letto-Scrittura;
* Di classe:
	* Insegnante di classe per Mondo delle Parole e Progetto Letto-Scrittura;
	
L'aggiunta di nuovi ruoli può essere fatta dinamicamente dal pannello amministrativo indicando il nome del ruolo, il livello di applicazione e i permessi da applicare agli utenti.


Accessi
=======
La sezione consente di monitorare gli accessi degli utenti al sistema.


Messaggi
========
La sezione messaggi mostra:
* Le notice: le notice rappresentano eventi generati dal sistema. Ogni notice viene analizzata da un processo asincrono e, a seconda delle configurazioni può generare una notice user, cioè un messaggio verso un utente specifico.
* Messaggi: riepilogo delle mail inviate dal sistema;
* Mail di test: pagina per testare il funzionamento dell'invio messaggi;


Sezione tecnica
===============
La sezione tecnica permette di consultare elementi di dettaglio della configurazione del sistema, tra questi:
	* Gestione dell'anno scolastico;
	* Valori di configurazione generale, tra i quali l'attivazione/disattivazione dei moduli;
	* Dizionari: con possibilità di consultare e editare tutti gli elenchi gestiti dal sistema;
	* Log di sistema;
	* Elenco degli indirizzi IP bloccati;
