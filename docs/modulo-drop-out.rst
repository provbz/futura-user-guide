﻿********
Drop Out
********

Il modulo nasce dalla volontà di strutturare e raccoglere le informazioni legate alla problematica dell'abbandono scolastico.
Il sistema permette di inserire segnalazioni di frequenze irregolari monitorando quindi situazioni di possibile rischio.

Sezione amministrativa
======================
Gli utenti con ruolo "Amministratore Drop Out" possono accedere al pannello amministrativo dedicato e ai dati presenti negli
Istituti relativi alle segnalazioni caricate dai referenti.
Il pannello amministrativo per il modulo consente di visualizzare e ricercare i dati per tutti le segnalazioni caricate negli istituti.
Inoltre, sono state predisposte le seguenti dashboard:

    * Riepilogo per grado scolastico
    * Riepilogo per genere
    * Riepilogo per cittadinanza

I dati delle dashboard vengono calcolati in tempo reale ad ogni nuova richiesta.

.. figure:: images/admin-e-model.jpg
	:width: 655px
	:align: center


Gestione a livello di istituto
==============================
A livello di istituto è stato predisposto il ruolo di:

	* Coordinatore di classe per il Drop Out

Che consente agli utenti di caricare e gestire le segnalazioni. 
Inoltre, come avviene per gli altri moduli, il dirigente di istituto e gli amministrativi hanno accesso alla visualizzazione dei documenti.

Elenco studenti segnalati
-------------------------
A livello di istituto è possibile per gli utenti autorizzati visualizzare l'elenco degli studenti per i quali viene segnalata uno stato di
frequenze discontinue.

.. figure:: images/structure-drop-out.jpg
	:width: 655px
	:align: center

È possibile creare nuove segnalazioni premendo il pulsante "Inserisci".
Per prima cosa verrà richiesto il codice POPCORN dello studente:

.. figure:: images/structure-drop-out-nuovo.jpg
	:width: 655px
	:align: center

Se il codice dello studente è associato ad un altro istituto verrà proposta la procedura di richiesta di trasferimento.
In tal caso sarà possibile procedere con la compilazione solo dopo l'approvazione del precedente istituto di appartenenza.
In alternativa, il sistema proporrà di completare i dati di anagrafica:

.. figure:: images/structure-drop-out-nuovo-anagrafica.jpg
	:width: 655px
	:align: center


Gestione delle segnalazioni
---------------------------
Dopo aver inserito i dati anagrafici dello studente sarà possibile accedere al registro delle segnalazioni raccolte per lo studente.

.. figure:: images/structure-drop-out-registro.jpg
	:width: 655px
	:align: center

Il registro offre le seguenti funzionalità:

**Modifica dei dati di anagrafica:** premendo questo pulsante si potranno modificare i dati anagrafici, il plesso e la classe dello studente.
Inserimento di una segnalazione di nuova assenza/frequenza irregolare: premendo questo pulsante si andranno ad inserire nuove segnalazioni di frequenze irregolari. 
Ogni segnalazione deve essere completata con il numero di giorni di assenza, il numero di giorni previsti di frequeza (totali) e l'azione intrapresa dalla scuola. 
Nel caso si inserisse una segnalazione di assenza con convocazione della famiglia lo stato passerà a "giallo".
Dopo aver inserito una segnalazione di assenza con segnalazione in procura lo stato dell'alunno nell'elenco principale passa a "rosso".

.. figure:: images/structure-drop-out-assenza-nuova.jpg
    :width: 655px
    :align: center

**Dichiarazione di ripresa frequenza:** permette di inserire segnalazioni di ripresa frequenza dell'alunno. 
Dopo aver inserito la segnalazione di "ripresa frequenza" lo stato dell'alunno nell'elenco principale passa a "verde".

**Invia i dati:** permette di inviare i dati raccolti ad un altro istituto presente sul sistema. 
La funzione permette di gestire i trasferimenti.

**Chiudi anno:** permette di inserire i dati di chiusura d'anno cioè: esito scolastico e stato di iscrizione all'anno scolastico successivo. 
Dopo aver inserito questi dati il documento viene chiuso e non sarà più possibile eseguire modifiche.

Tutte le segnalazioni vengono conservate nel riepilogo dello studente:

.. figure:: images/structure-drop-out-segnalazione.jpg
	:width: 655px
	:align: center

Ogni segnalazione viene mantenuta e non risulta più modificabile dopo l'inserimento. Solo i dati di chiusura d'anno possono essere modificati in istanti successivi.
In fase di inserimento di una nuova assenza verrà richiesto di specificare l'azione intrapresa dalla scuola.
A seconda dell'azione intrapresa, lo stato dello studente verrà evidenziato con un segnalatore: verde, giallo o rosso.
In caso si registrasse una segnalazione in procura, il sistema permetterà di scaricare il modello della lettera di segnalazione.

.. figure:: images/structure-drop-out-lettera-procura.jpg
	:width: 655px
	:align: center